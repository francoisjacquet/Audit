<?php
/**
 * Audit functions
 *
 * @package Audit
 */

/**
 * DB Query action: save SQL query to later save it in the Audit log
 *
 * @global array $_ROSARIO['AuditLog']
 *
 * @param string $tag 'database.inc.php|dbquery_after' hook.
 * @param string $sql SQL query from DBGet function.
 *
 * @return boolean False if not INSERT, UPATE or DELETE.
 */
function AuditLogDBQuery( $tag, $sql ) {

	global $_ROSARIO;

	if ( stripos( $sql, 'INSERT ' ) !== 0
		&& stripos( $sql, 'UPDATE ' ) !== 0
		&& stripos( $sql, 'DELETE ' ) !== 0 )
	{
		// Is SELECT.
		return false;
	}

	if ( empty( $_ROSARIO['AuditLog'] ) )
	{
		$_ROSARIO['AuditLog'] = [
			'INSERT' => [],
			'UPDATE' => [],
			'DELETE' => [],
		];
	}

	if ( stripos( $sql, 'INSERT ' ) === 0 )
	{
		$_ROSARIO['AuditLog']['INSERT'][] = $sql;
	}
	elseif ( stripos( $sql, 'UPDATE ' ) === 0 )
	{
		$_ROSARIO['AuditLog']['UPDATE'][] = $sql;
	}
	elseif ( stripos( $sql, 'DELETE ' ) === 0 )
	{
		$_ROSARIO['AuditLog']['DELETE'][] = $sql;
	}

	return true;
}

add_action( 'database.inc.php|dbquery_after', 'AuditLogDBQuery', 2 );


/**
 * Prepared / parameterized DB Query action: save SQL query to later save it in the Audit log
 *
 * @global array $_ROSARIO['AuditLog']
 *
 * @since RosarioSIS 14.0
 *
 * @param string $tag    'database.inc.php|dbqueryprep_after' hook.
 * @param string $sql    Prepared SQL statement / parameterized query from DBGetPrep function.
 * @param array  $params Filtered parameter values from DBGetPrep function.
 *
 * @return boolean False if not INSERT, UPATE or DELETE.
 */
function AuditLogDBQueryPrep( $tag, $sql, $params ) {

	global $_ROSARIO;

	if ( stripos( $sql, 'INSERT ' ) !== 0
		&& stripos( $sql, 'UPDATE ' ) !== 0
		&& stripos( $sql, 'DELETE ' ) !== 0 )
	{
		// Is SELECT.
		return false;
	}

	if ( empty( $_ROSARIO['AuditLog'] ) )
	{
		$_ROSARIO['AuditLog'] = [
			'INSERT' => [],
			'UPDATE' => [],
			'DELETE' => [],
		];
	}

	if ( $params
		&& function_exists( 'db_sql_prep_emulate' ) )
	{
		$sql = db_sql_prep_emulate( $sql, $params );
	}

	if ( stripos( $sql, 'INSERT ' ) === 0 )
	{
		$_ROSARIO['AuditLog']['INSERT'][] = $sql;
	}
	elseif ( stripos( $sql, 'UPDATE ' ) === 0 )
	{
		$_ROSARIO['AuditLog']['UPDATE'][] = $sql;
	}
	elseif ( stripos( $sql, 'DELETE ' ) === 0 )
	{
		$_ROSARIO['AuditLog']['DELETE'][] = $sql;
	}

	return true;
}

add_action( 'database.inc.php|dbqueryprep_after', 'AuditLogDBQueryPrep', 3 );



/**
 * Warehouse Footer action: save the Audit Log SQL queries if any.
 *
 * @global array $_ROSARIO['AuditLog']
 *
 * @uses db_query and not DBQuery or we would end up in an infinite loop.
 *
 * @return False if no SQL queries to log.
 */
function AuditLogWarehouseFooterSave()
{
	global $_ROSARIO;

	if ( empty( $_ROSARIO['AuditLog'] )
		|| ! User( 'USERNAME' ) )
	{
		return false;
	}

	$sql_audit_log = '';

	foreach ( (array) $_ROSARIO['AuditLog'] as $query_type => $sql_queries )
	{
		if ( empty( $sql_queries ) )
		{
			continue;
		}

		$sql = implode( ";\n", $sql_queries );

		// Remove tabulations.
		$sql = str_replace( "\t", '', $sql );

		if ( ROSARIO_DEBUG )
		{
			echo '<!-- ' . print_r( $sql, true ) . ' -->';
		}

		$sql_audit_log .= "INSERT INTO audit_log(SYEAR,USERNAME,PROFILE,URL,QUERY_TYPE,DATA)
		VALUES('" . Config( 'SYEAR' ) . "','" . User( 'USERNAME' ) . "','" . User( 'PROFILE' ) . "','" .
		DBEscapeString( $_SERVER['REQUEST_URI'] ) . "','" . $query_type . "','" . DBEscapeString( $sql ) . "');";
	}

	if ( ! $sql_audit_log )
	{
		return false;
	}

	db_query( $sql_audit_log );

	return true;
}

add_action( 'Warehouse.php|footer', 'AuditLogWarehouseFooterSave', 0 );


if ( isset( $_REQUEST['modfunc'] ) )
{
	global $_ROSARIO; // Must declare global here as we are inside _LoadAddons() function...

	// Save modfunc (before RedirectURL()), for later use in AuditBrowsingLogWarehouseFooterSave()
	$_ROSARIO['AuditBrowsingLogWarehouseFooterSave']['modfunc'] = $_REQUEST['modfunc'];
}

/**
 * Warehouse Footer action: save the Browsing Log if relevant.
 *
 * @global $_ROSARIO
 *
 * @return False if not browsing Modules.php or Bottom.php (Print) button.
 */
function AuditBrowsingLogWarehouseFooterSave( $tag )
{
	global $_ROSARIO;

	$script = basename( $_SERVER['SCRIPT_NAME'] );

	if ( ! User( 'USERNAME' )
		|| ! UserSchool()
		|| empty( $_REQUEST['modname'] )
		|| ! in_array( $script, [ 'Modules.php', 'Bottom.php' ] ) )
	{
		return false;
	}

	if ( ! Config( 'AUDIT_LOG_BROWSING' ) )
	{
		return false;
	}

	if ( mb_strpos( $tag, 'pdf_stop' )
		&& $script !== 'Bottom.php' )
	{
		return false;
	}

	$action = $script === 'Bottom.php' ? DBEscapeString( _( 'Print' ) ) : '';

	if ( $script === 'Modules.php'
		&& ! empty( $_ROSARIO['AuditBrowsingLogWarehouseFooterSave']['modfunc'] ) )
	{
		$action = $_ROSARIO['AuditBrowsingLogWarehouseFooterSave']['modfunc'];
	}

	$user_staff_id = UserStaffID() ? "'" . UserStaffID() . "'" : 'NULL';
	$user_student_id = User( 'PROFILE' ) !== 'student' && UserStudentID() ? "'" . UserStudentID() . "'" : 'NULL';

	$browsing_log = "INSERT INTO browsing_log(SCHOOL_ID,USERNAME,PROFILE,URL,PROGRAM_TITLE,
		ACTION,CURRENT_STAFF_ID,CURRENT_STUDENT_ID)
	VALUES('" . UserSchool() . "','" . User( 'USERNAME' ) . "','" . User( 'PROFILE' ) . "','" .
	DBEscapeString( $_SERVER['REQUEST_URI'] ) . "','" . DBEscapeString( ProgramTitle() ) . "','" .
	$action . "'," . $user_staff_id . "," . $user_student_id . ");";

	db_query( $browsing_log );

	return true;
}

add_action( 'Warehouse.php|footer', 'AuditBrowsingLogWarehouseFooterSave', 1 );

add_action( 'functions/PDF.php|pdf_stop_pdf', 'AuditBrowsingLogWarehouseFooterSave', 1 );
add_action( 'functions/PDF.php|pdf_stop_html', 'AuditBrowsingLogWarehouseFooterSave', 1 );
