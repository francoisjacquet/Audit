<?php
/**
 * Browsing Log
 * Display Browsing Log records
 *
 * @package Audit
 */

// Set start date.
$start_date = RequestedDate( 'start', date( 'Y-m-d', time() - 60 * 60 * 24 ) );

// Set end date.
$end_date = RequestedDate( 'end', DBDate() );

if ( $_REQUEST['modfunc'] === 'save' )
{
	if ( isset( $_REQUEST['AUDIT_LOG_BROWSING'] ) )
	{
		Config( 'AUDIT_LOG_BROWSING', $_REQUEST['AUDIT_LOG_BROWSING'] );
	}

	// Unset AUDIT_LOG_BROWSING, modfunc & redirect URL.
	RedirectURL( [ 'AUDIT_LOG_BROWSING', 'modfunc' ] );
}

if ( $_REQUEST['modfunc'] === 'delete' )
{
	// Prompt before deleting log.
	if ( DeletePrompt( dgettext( 'Audit', 'Browsing Log' ) ) )
	{
		DBQuery( 'DELETE FROM browsing_log' );

		$note[] = dgettext( 'Audit', 'Browsing Log cleared.' );

		// Unset modfunc & redirect URL.
		RedirectURL( 'modfunc' );
	}
}

echo ErrorMessage( $note, 'note' );

if ( ! $_REQUEST['modfunc'] )
{
	if ( ! AllowEdit() )
	{
		$_ROSARIO['allow_edit'] = true;

		$allow_edit_tmp = true;
	}

	$header_left = '<form action="' . PreparePHP_SELF() . '" method="GET">';

	$header_left .= _( 'From' ) . ' ' . DateInput( $start_date, 'start', '', false, false ) . ' - ' .
		_( 'To' ) . ' ' . DateInput( $end_date, 'end', '', false, false ) .
		Buttons( _( 'Go' ) );

	$header_left .= '</form>';

	$header_right = '<form action="' . PreparePHP_SELF( [], [], [ 'modfunc' => 'save' ] ) . '" method="POST">';

	$header_right .= CheckboxInput(
		Config( 'AUDIT_LOG_BROWSING' ),
		'AUDIT_LOG_BROWSING',
		dgettext( 'Audit', 'Enable Browsing Log' ),
		'',
		true,
		'Yes',
		'No',
		false,
		( version_compare( ROSARIO_VERSION, '12.5', '>=' ) ?
			// @since RosarioSIS 12.5 CSP remove unsafe-inline Javascript
			'class="onchange-ajax-post-form"' :
			'onchange="ajaxPostForm(this.form, true);"' )
	);

	$header_right .= '</form>';

	DrawHeader(
		$header_left,
		$header_right
	);

	if ( ! empty( $allow_edit_tmp ) )
	{
		$_ROSARIO['allow_edit'] = false;
	}


	// Format DB data.
	$browsing_logs_functions = [
		'PROGRAM_TITLE' => '_makeAuditLogProgram', // Add link (URL) to Program Title.
		'PROFILE' => 'AuditLogMakeProfile', // Translate profile.
		'USERNAME' => 'AuditLogMakeUsername', // Add link to user info.
		'CREATED_AT' => 'ProperDateTime', // Display localized & preferred Date & Time.
		'CURRENT_STUDENT_STAFF' => '_makeAuditLogCurrentStudentStaff', // Display Working Student / User Name (ID).
	];

	if ( function_exists( 'SQLLimitForList' ) )
	{
		// @since RosarioSIS 11.7 Limit SQL result for List
		$sql_count = "SELECT COUNT(1)
			FROM browsing_log
			WHERE CREATED_AT >='" . $start_date . "'
			AND CREATED_AT <='" . $end_date . ' 23:59:59' . "'";

		$sql_limit = SQLLimitForList( $sql_count );
	}
	else
	{
		$sql_limit = " LIMIT 1000";
	}

	$browsing_logs_RET = DBGet( "SELECT USERNAME,PROFILE,CREATED_AT,URL,
		PROGRAM_TITLE,ACTION,CURRENT_STAFF_ID,CURRENT_STUDENT_ID,'' AS CURRENT_STUDENT_STAFF
		FROM browsing_log
		WHERE CREATED_AT >='" . $start_date . "'
		AND CREATED_AT <='" . $end_date . ' 23:59:59' . "'
		ORDER BY CREATED_AT DESC" . $sql_limit, $browsing_logs_functions );

	echo '<form action="' . ( function_exists( 'URLEscape' ) ?
		URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&tab=browsing&modfunc=delete' ) :
		_myURLEncode( 'Modules.php?modname=' . $_REQUEST['modname'] . '&tab=browsing&modfunc=delete' ) ) . '" method="POST">';

	DrawHeader( '', SubmitButton( _( 'Clear Log' ), '', '' ) );

	$warning[] = dgettext( 'Audit', 'Browsing records will fill the database significantly.' );

	echo ErrorMessage( $warning, 'warning' );

	ListOutput(
		$browsing_logs_RET,
		[
			'CREATED_AT' => _( 'Date' ),
			'USERNAME' => _( 'Username' ),
			'PROFILE' => _( 'User Profile' ),
			'PROGRAM_TITLE' => dgettext( 'Audit', 'Program' ),
			'ACTION' => dgettext( 'Audit', 'Action' ),
			'CURRENT_STUDENT_STAFF' => dgettext( 'Audit', 'Working Student / User' ),
		],
		dgettext( 'Audit', 'Browsing record' ),
		dgettext( 'Audit', 'Browsing records' ),
		[],
		[],
		[
			'valign-middle' => true,
			// @since 10.9 Add pagination for list > 1000 results
			'pagination' => true,
		]
	);

	echo '</form>';

	// When clicking on Username, go to Student or User Info.
	echo '<script src="modules/Audit/js/AuditLog.js?v=11.1"></script>';
}


/**
 * Make Working Student / User
 * Name (ID)
 *
 * Local function
 * DBGet callback
 *
 * @param  string $value   Field value.
 * @param  string $name    'CURRENT_STUDENT_STAFF'.
 *
 * @return string
 */
function _makeAuditLogCurrentStudentStaff( $value, $column )
{
	global $THIS_RET;

	if ( empty( $THIS_RET['CURRENT_STAFF_ID'] )
		&& empty( $THIS_RET['CURRENT_STUDENT_ID'] ) )
	{
		return '';
	}

	$staff = $student = '';

	if ( ! empty( $THIS_RET['CURRENT_STAFF_ID'] ) )
	{
		$staff = DBGetOne( "SELECT " . DisplayNameSQL() . "
			FROM staff
			WHERE STAFF_ID='" . (int) $THIS_RET['CURRENT_STAFF_ID'] . "'" );

		$staff .= ' (' . $THIS_RET['CURRENT_STAFF_ID'] . ')';
	}

	if ( ! empty( $THIS_RET['CURRENT_STUDENT_ID'] ) )
	{
		$student = DBGetOne( "SELECT " . DisplayNameSQL() . "
			FROM students
			WHERE STUDENT_ID='" . (int) $THIS_RET['CURRENT_STUDENT_ID'] . "'" );

		$student .= ' (' . $THIS_RET['CURRENT_STUDENT_ID'] . ')';
	}

	return $student . ' / ' . $staff;
}


/**
 * Make Working Student / User
 * Name (ID)
 *
 * Local function
 * DBGet callback
 *
 * @param  string $value   Field value.
 * @param  string $name    'CURRENT_STUDENT_STAFF'.
 *
 * @return string
 */
function _makeAuditLogProgram( $value, $column )
{
	global $THIS_RET;

	if ( empty( $THIS_RET['URL'] ) )
	{
		return $value;
	}

	return '<a href="' . ( function_exists( 'URLEscape' ) ? URLEscape( $THIS_RET['URL'] ) : _myURLEncode( $THIS_RET['URL'] ) ) . '" target="_blank">' . $value . '</a>';
}
