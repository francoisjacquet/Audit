<?php
/**
 * SQL Log
 * Display SQL Log records
 *
 * @package Audit
 */

// Set start date.
$start_date = RequestedDate( 'start', date( 'Y-m-d', time() - 60 * 60 * 24 ) );

// Set end date.
$end_date = RequestedDate( 'end', DBDate() );

if ( $_REQUEST['modfunc'] === 'delete' )
{
	// Prompt before deleting log.
	if ( DeletePrompt( dgettext( 'Audit', 'SQL Log' ) ) )
	{
		DBQuery( 'DELETE FROM audit_log' );

		$note[] = dgettext( 'Audit', 'SQL Log cleared.' );

		// Unset modfunc & redirect URL.
		RedirectURL( 'modfunc' );
	}
}

echo ErrorMessage( $note, 'note' );

if ( ! $_REQUEST['modfunc'] )
{
	echo '<form action="' . PreparePHP_SELF() . '" method="GET">';

	if ( ! AllowEdit() )
	{
		$_ROSARIO['allow_edit'] = true;

		$allow_edit_tmp = true;
	}

	DrawHeader(
		_( 'From' ) . ' ' . DateInput( $start_date, 'start', '', false, false ) . ' - ' .
		_( 'To' ) . ' ' . DateInput( $end_date, 'end', '', false, false ) .
		Buttons( _( 'Go' ) )
	);

	if ( ! empty( $allow_edit_tmp ) )
	{
		$_ROSARIO['allow_edit'] = false;
	}

	echo '</form>';

	// Format DB data.
	$audit_logs_functions = [
		'URL' => 'AuditLogMakeURL', // Add link to URL.
		'PROFILE' => 'AuditLogMakeProfile', // Translate profile.
		'USERNAME' => 'AuditLogMakeUsername', // Add link to user info.
		'CREATED_AT' => 'ProperDateTime', // Display localized & preferred Date & Time.
		'QUERY_TYPE' => '_makeAuditLogQueryType', // Display DELETE in red, INSERT in green.
		'DATA' => '_makeAuditLogData', // Display SQL data.
	];

	if ( function_exists( 'SQLLimitForList' ) )
	{
		// @since RosarioSIS 11.7 Limit SQL result for List
		$sql_count = "SELECT COUNT(1)
			FROM audit_log
			WHERE CREATED_AT >='" . $start_date . "'
			AND CREATED_AT <='" . $end_date . ' 23:59:59' . "'";

		$sql_limit = SQLLimitForList( $sql_count );
	}
	else
	{
		$sql_limit = " LIMIT 1000";
	}

	$audit_logs_RET = DBGet( "SELECT
		USERNAME,PROFILE,CREATED_AT,URL,QUERY_TYPE,DATA
		FROM audit_log
		WHERE CREATED_AT >='" . $start_date . "'
		AND CREATED_AT <='" . $end_date . ' 23:59:59' . "'
		ORDER BY CREATED_AT DESC" . $sql_limit, $audit_logs_functions );

	echo '<form action="' . ( function_exists( 'URLEscape' ) ?
		URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&modfunc=delete' ) :
		_myURLEncode( 'Modules.php?modname=' . $_REQUEST['modname'] . '&modfunc=delete' ) ) . '" method="POST">';

	DrawHeader( '', SubmitButton( _( 'Clear Log' ), '', '' ) );

	ListOutput(
		$audit_logs_RET,
		[
			'CREATED_AT' => _( 'Date' ),
			'USERNAME' => _( 'Username' ),
			'PROFILE' => _( 'User Profile' ),
			'URL' => dgettext( 'Audit', 'URL' ),
			'QUERY_TYPE' => _( 'Type' ),
			'DATA' => '<span class="a11y-hidden">' . _( 'Data' ) . '</span>',
		],
		dgettext( 'Audit', 'SQL record' ),
		dgettext( 'Audit', 'SQL records' ),
		[],
		[],
		[
			'valign-middle' => true,
			// @since 10.9 Add pagination for list > 1000 results
			'pagination' => true,
		]
	);

	echo '</form>';

	// When clicking on Username, go to Student or User Info.
	echo '<script src="modules/Audit/js/AuditLog.js?v=11.1"></script>';
}


/**
 * Make Query Type
 * Display DELETE in red, INSERT in green.
 *
 * Local function
 * DBGet callback
 *
 * @param  string $value   Field value.
 * @param  string $name    'QUERY_TYPE'.
 *
 * @return string          Username linking to user info page.
 */
function _makeAuditLogQueryType( $value, $column )
{
	global $THIS_RET;

	if ( ! $value )
	{
		return '';
	}

	if ( isset( $_REQUEST['_ROSARIO_PDF'] ) )
	{
		return $value;
	}

	$return = $value;

	if ( $value === 'DELETE' )
	{
		$return = '<span style="color: red;">' . $value . '</a>';
	}
	elseif ( $value === 'INSERT' )
	{
		$return = '<span style="color: green;">' . $value . '</a>';
	}

	return $return;
}


/**
 * Display data
 * Display SQL queries + count if > 1 inside ColorBox.
 *
 * Local function
 * DBGet callback
 *
 * @param  string $value   Field value.
 * @param  string $name    'DATA'.
 *
 * @return string          SQL queries ColorBox.
 */
function _makeAuditLogData( $value, $column )
{
	global $THIS_RET;

	static $i = 1;

	if ( ! $value )
	{
		return '';
	}

	if ( isset( $_REQUEST['_ROSARIO_PDF'] ) )
	{
		return $value;
	}

	$query_type = $THIS_RET['QUERY_TYPE'];

	// Count queries.
	$count_queries = substr_count( $value, $query_type );

	$count_queries_html = '';

	if ( $count_queries > 1 )
	{
		$query_type_color = _makeAuditLogQueryType( $query_type, '' );

		$count_queries_html = '<p>' . $count_queries . ' ' . $query_type_color . '</p>';
	}

	$return = '<div style="display:none;"><div id="' . $column . $i . '" class="colorboxinline">' .
		$count_queries_html .
		'<pre><code>' . $value . '</code></pre></div></div>';

	$return .= button(
		'visualize',
		'',
		'"#' . $column . $i++ . '" class="colorboxinline"',
		'bigger'
	);

	return $return;
}
