<?php
/**
 * Common functions
 *
 * @package Audit module
 */

/**
 * Make URL
 * Add link to URL
 *
 * DBGet callback
 *
 * @param  string $value   Field value.
 * @param  string $name    'URL'.
 *
 * @return string          URL with HTML link.
 */
function AuditLogMakeURL( $value, $column )
{
	if ( ! $value )
	{
		return '';
	}

	if ( isset( $_REQUEST['_ROSARIO_PDF'] ) )
	{
		return $value;
	}

	// Truncate links > 80 chars.
	$truncated_link = $value;

	if ( mb_strpos( $truncated_link, 'Modules.php' ) )
	{
		// Remove directories before Modules.php.
		$truncated_link = mb_substr( $truncated_link, mb_strpos( $truncated_link, 'Modules.php' ) );
	}

	if ( mb_strpos( $truncated_link, 'Bottom.php' ) )
	{
		// Remove directories before Bottom.php.
		$truncated_link = mb_substr( $truncated_link, mb_strpos( $truncated_link, 'Bottom.php' ) );
	}

	if ( mb_strlen( $truncated_link ) > 80 )
	{
		$separator = '/.../';
		$separator_length = mb_strlen( $separator );
		$max_length = 80 - $separator_length;
		$start = (int) ( $max_length / 2 );
		$trunc =  mb_strlen( $truncated_link ) - $max_length;
		$truncated_link = substr_replace( $truncated_link, $separator, (int) $start, (int) $trunc );
	}

	return '<a href="' . ( function_exists( 'URLEscape' ) ? URLEscape( $value ) : _myURLEncode( $value ) ) . '" target="_blank">' . $truncated_link . '</a>';
}


/**
 * Make Profile
 * Only for successful logins.
 *
 * DBGet callback
 *
 * @param  string $value   Field value.
 * @param  string $name    'PROFILE'.
 *
 * @return string          Student, Administrator, Teacher, Parent, or No Access.
 */
function AuditLogMakeProfile( $value, $column )
{
	$profile_options = [
		'student' => _( 'Student' ),
		'admin' => _( 'Administrator' ),
		'teacher' => _( 'Teacher' ),
		'parent' => _( 'Parent' ),
		'none' => _( 'No Access' ),
	];

	if ( ! isset( $profile_options[ $value ] ) )
	{
		return '';
	}

	return $profile_options[ $value ];
}


/**
 * Make Username
 * Links to user info page.
 *
 * DBGet callback
 *
 * @param  string $value   Field value.
 * @param  string $name    'USERNAME'.
 *
 * @return string          USername linking to user info page.
 */
function AuditLogMakeUsername( $value, $column )
{
	global $THIS_RET;

	if ( ! $value )
	{
		return '';
	}

	if ( isset( $_REQUEST['_ROSARIO_PDF'] ) )
	{
		return $value;
	}

	return '<a class="al-username ' .
		( $THIS_RET['PROFILE'] === 'student' ? 'student' : '' ) .
		'" href="#">' . $value . '</a>';
}
