<?php
/**
 * Update database
 *
 * @package Slovenian Class Diary module
 */

global $DatabaseType;

// @since 11.0 Add Browsing Log
$browsing_log_table_exist = DBGetOne( "SELECT 1
	FROM information_schema.tables
	WHERE table_schema=" . ( ! empty( $DatabaseType ) && $DatabaseType === 'mysql' ? 'DATABASE()' : 'CURRENT_SCHEMA()' ) . "
	AND table_name='browsing_log';" );

$update_sql_file = ( ! empty( $DatabaseType ) && $DatabaseType === 'mysql' ?
	'modules/Audit/update11.0_mysql.sql' :
	'modules/Audit/update11.0.sql' );

if ( ! $browsing_log_table_exist
	&& file_exists( $update_sql_file ) )
{
	// SQL Update to 11.0.
	$update_sql = file_get_contents( $update_sql_file );

	db_query( $update_sql );
}
