/**
 * Audit Log program JS
 *
 * @package Audit module
 */

// When clicking on Username, go to Student or User Info.
$('.al-username').attr('href', function(){
	var url = 'Modules.php?modname=Users/User.php&search_modfunc=list&';

	if ( $(this).hasClass('student') ) {
		url = url.replace( 'Users/User.php', 'Students/Student.php' ) + 'cust[USERNAME]=';
	} else {
		url += 'username=';
	}

	return url + this.firstChild.data;
});
