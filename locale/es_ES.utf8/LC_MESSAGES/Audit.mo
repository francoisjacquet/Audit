��          �      ,      �     �     �  	   �     �     �     �     �  6   �          /     7     ?  
   P     [     g     k    �     �  
   �     �     �      �     �       P     "   p     �     �     �     �     �     �     �                     
            	                                             Action Audit Audit Log Browsing Log Browsing Log cleared. Browsing record Browsing records Browsing records will fill the database significantly. Enable Browsing Log Program SQL Log SQL Log cleared. SQL record SQL records URL Working Student / User Project-Id-Version: Audit module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-09-16 13:20+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2;ngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Acción Auditoría Registro de Auditoría Registro de navegación Registro de navegación borrado. Registro de navegación Registros de navegación Los registros de navegación llenarán la base de datos de manera significativa. Activar el registro de navegación Programa Registro SQL Registro SQL borrado. Registro SQL Registros SQL URL Estudiante / Usuario corriente 