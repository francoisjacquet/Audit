��          �      ,      �     �     �  	   �     �     �     �     �  6   �          /     7     ?  
   P     [     g     k  �  �     m     t     z     �     �     �     �  L   �      9  	   Z     d     p     �     �     �     �                     
            	                                             Action Audit Audit Log Browsing Log Browsing Log cleared. Browsing record Browsing records Browsing records will fill the database significantly. Enable Browsing Log Program SQL Log SQL Log cleared. SQL record SQL records URL Working Student / User Project-Id-Version: Audit module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-09-16 13:19+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2;ndgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 Action Audit Journal d'audit Journal de navigation Journal de navigation effacé. Entrée de navigation Entrées de navigation Les entrées de navigation rempliront significativement la base de données. Activer le journal de navigation Programme Journal SQL Journal SQL effacé. Entrée SQL Entrées SQL URL Élève / Utilisateur courant 