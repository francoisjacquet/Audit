/**
 * Update 11.0 SQL (PostgreSQL)
 * Add Browsing Log
 *
 * @package Audit module
 */

--
-- Fix #102 error language "plpgsql" does not exist
--

CREATE OR REPLACE LANGUAGE plpgsql;

/**
 * Browsing Log table
 */
--
-- Name: browsing_log; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE TABLE IF NOT EXISTS browsing_log (
    id serial PRIMARY KEY,
    school_id integer NOT NULL,
    username varchar(100),
    profile varchar(30),
    url text,
    program_title text,
    action varchar(50),
    current_staff_id integer,
    current_student_id integer,
    created_at timestamp DEFAULT current_timestamp
);
