<?php
/**
 * Audit Log
 * Display SQL or Browsing Log records
 *
 * @package Audit
 */

require_once 'modules/Audit/includes/common.fnc.php';

DrawHeader( ProgramTitle() );

if ( empty( $_REQUEST['tab'] ) )
{
	$_REQUEST['tab'] = 'sql';
}

$sql_link = '<a href="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] ) . '">' .
	( $_REQUEST['tab'] !== 'browsing' ?
	'<b>' . dgettext( 'Audit', 'SQL Log' ) . '</b>' : dgettext( 'Audit', 'SQL Log' ) ) . '</a>';

$browsing_link = ' | <a href="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&tab=browsing' ) . '">' .
	( $_REQUEST['tab'] === 'browsing' ?
	'<b>' . dgettext( 'Audit', 'Browsing Log' ) . '</b>' : dgettext( 'Audit', 'Browsing Log' ) ) . '</a>';

DrawHeader( $sql_link . $browsing_link );

if ( $_REQUEST['tab'] === 'sql' )
{
	require_once 'modules/Audit/includes/SQLLog.inc.php';
}
elseif ( $_REQUEST['tab'] === 'browsing' )
{
	require_once 'modules/Audit/includes/BrowsingLog.inc.php';
}
